export declare class ExternalServices {
    static getProduct(environment: string, jwt_key: string, jwt_secret: string, productId: string, headers: any): Promise<any>;
}

export declare function generateJWT(key: string, secret: string, expiration?: number): string;

export declare class Logger {
    static info(headers: any, message: any): void;
    static error(headers: any, message: any): void;
}

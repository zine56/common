export declare class ProductRequest {
    eventId: string;
    eventType: string;
    entityId: string;
    entityType: string;
    timestamp: string;
    datetime: string;
    version: string;
    country: string;
    commerce: string;
    channel: string;
    domain: string;
    capability: string;
    mimeType: string;
    data: string;
}

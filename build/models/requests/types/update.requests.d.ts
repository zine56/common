export declare class UpdateRequest {
    constructor(sku: string, sat: string, um: string);
    sku: string;
    sat: string;
    um: string;
}
export declare class CommonModule {
}

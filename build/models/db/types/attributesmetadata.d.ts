import { AttributesClasses } from "./attributeclasses";
import { Attributes } from "./attributes";
export declare class AttributesMetadata {
    attribute_metadata_id?: number;
    attribute_class_id?: number;
    attribute_id?: string;
    value?: string;
    valueid?: string;
    attributes?: Attributes;
    attributes_classes?: AttributesClasses;
}

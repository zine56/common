import { AttributesClasses } from "./attributeclasses";
import { AttributesMetadata } from "./attributesmetadata";
import { ReferencessMetadata } from "./referencessmetadata";
import { Masters } from "./masters";
export declare class Attributes {
    attribute_id?: string;
    attribute_name?: string;
    lov_name?: string;
    attribute_multivalue?: string;
    attributes_classes?: AttributesClasses[];
    attributes_metadata?: AttributesMetadata[];
    referencess_metadata?: ReferencessMetadata[];
    masters?: Masters[];
}

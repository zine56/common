import { Attributes } from "./attributes";
import { Products } from "./products";
import { Values } from "./values";
export declare class Masters {
    master_id?: string;
    product_id?: string;
    attribute_id?: string;
    category?: string;
    attributes?: Attributes;
    products?: Products;
    values?: Values[];
}

import { Attributes } from "./attributes";
import { AttributesMetadata } from "./attributesmetadata";
import { Bricks } from "./bricks";
export declare class AttributesClasses {
    attribute_class_id?: number;
    attribute_id?: string;
    brick_id?: string;
    attributes?: Attributes;
    bricks?: Bricks;
    attributes_metadata?: AttributesMetadata[];
}

import { Attributes } from "./attributes";
import { Measures } from "./measures";
import { Referencess } from "./referencess";
export declare class ReferencessMetadata {
    reference_metadata_id?: number;
    reference_id?: number;
    attribute_id?: string;
    value?: string;
    valueid?: string;
    measure_id?: string;
    referencess?: Referencess;
    measures?: Measures;
    attributes?: Attributes;
}

import { Masters } from "./masters";
import { Measures } from "./measures";
export declare class Values {
    value_id?: number;
    value?: string;
    master_id?: number;
    lov_id?: string;
    measure_id?: string;
    masters?: Masters;
    measures?: Measures;
}

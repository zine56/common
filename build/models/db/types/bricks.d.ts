import { AttributesClasses } from "./attributeclasses";
import { Products } from "./products";
export declare class Bricks {
    brick_id?: string;
    brick_name?: string;
    products?: Products[];
    attributes_classes?: AttributesClasses[];
}

import { Bricks } from "./bricks";
import { Referencess } from "./referencess";
import { Masters } from "./masters";
export declare class Products {
    product_id?: string;
    product_name?: string;
    brick_id?: string;
    bricks?: Bricks;
    referencess?: Referencess[];
    masters?: Masters[];
}

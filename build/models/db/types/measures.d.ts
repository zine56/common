import { Values } from "./values";
import { ReferencessMetadata } from "./referencessmetadata";
export declare class Measures {
    measure_id?: string;
    measure_name?: string;
    values?: Values[];
    referencess_metadata?: ReferencessMetadata[];
}

import { Products } from "./products";
import { ReferencessMetadata } from "./referencessmetadata";
export declare class Referencess {
    reference_id?: number;
    target?: string;
    target_type?: string;
    product_id?: string;
    reference_type?: string;
    products?: Products;
    referencess_metadata?: ReferencessMetadata[];
}

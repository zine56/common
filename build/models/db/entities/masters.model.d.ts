import { Model } from 'sequelize-typescript';
import { attributes } from './attributes.model';
import { products } from './products.model';
import { values } from './values.model';
export declare class masters extends Model<masters> {
    master_id: string;
    product_id: string;
    attribute_id: string;
    category: string;
    attributes: attributes;
    products: products;
    values: values[];
}

import { Model } from 'sequelize-typescript';
import { products } from './products.model';
import { referencess_metadata } from './referencess_metadata.model';
export declare class referencess extends Model<referencess> {
    reference_id: number;
    target: string;
    target_type: string;
    product_id: string;
    reference_type: string;
    products: products;
    referencess_metadata: referencess_metadata[];
}

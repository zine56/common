import { Model } from 'sequelize-typescript';
import { referencess } from './referencess.model';
import { attributes } from './attributes.model';
import { measures } from './measures.model';
export declare class referencess_metadata extends Model<referencess_metadata> {
    reference_metadata_id: number;
    reference_id: number;
    attribute_id: string;
    value: string;
    valueid: string;
    measure_id: string;
    referencess: referencess;
    measures: measures;
    attributes: attributes;
}

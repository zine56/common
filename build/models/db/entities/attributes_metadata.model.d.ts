import { Model } from 'sequelize-typescript';
import { attributes } from './attributes.model';
import { attributes_classes } from './attributes_classes.model';
export declare class attributes_metadata extends Model<attributes_metadata> {
    attribute_metadata_id: number;
    attribute_class_id: number;
    attribute_id: string;
    value: string;
    valueid: string;
    attributes: attributes;
    attributes_classes: attributes_classes;
}

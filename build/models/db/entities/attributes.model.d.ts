import { Model } from 'sequelize-typescript';
import { attributes_classes } from './attributes_classes.model';
import { attributes_metadata } from './attributes_metadata.model';
import { masters } from './masters.model';
import { referencess_metadata } from './referencess_metadata.model';
export declare class attributes extends Model<attributes> {
    attribute_id: string;
    attribute_name: string;
    lov_name: string;
    attribute_multivalue: string;
    attributes_classes: attributes_classes[];
    attributes_metadata: attributes_metadata[];
    referencess_metadata: referencess_metadata[];
    masters: masters[];
}

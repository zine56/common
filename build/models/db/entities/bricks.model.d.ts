import { Model } from 'sequelize-typescript';
import { products } from './products.model';
import { attributes_classes } from './attributes_classes.model';
export declare class bricks extends Model<bricks> {
    brick_id: string;
    brick_name: string;
    products: products[];
    attributes_classes: attributes_classes[];
}

import { Model } from 'sequelize-typescript';
import { attributes } from './attributes.model';
import { bricks } from './bricks.model';
import { attributes_metadata } from './attributes_metadata.model';
export declare class attributes_classes extends Model<attributes_classes> {
    attribute_class_id: number;
    attribute_id: string;
    brick_id: string;
    attributes: attributes;
    bricks: bricks;
    attributes_metadata: attributes_metadata[];
}

import { Model } from 'sequelize-typescript';
import { masters } from './masters.model';
import { measures } from './measures.model';
export declare class values extends Model<values> {
    value_id: number;
    value: string;
    master_id: number;
    lov_id: string;
    measure_id: string;
    masters: masters;
    measures: measures;
}

import { Model } from 'sequelize-typescript';
import { bricks } from './bricks.model';
import { referencess } from './referencess.model';
import { masters } from './masters.model';
export declare class products extends Model<products> {
    product_id: string;
    product_name: string;
    brick_id: string;
    bricks: bricks;
    referencess: referencess[];
    masters: masters[];
}

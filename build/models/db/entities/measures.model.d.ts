import { Model } from 'sequelize-typescript';
import { values } from './values.model';
import { referencess_metadata } from './referencess_metadata.model';
export declare class measures extends Model<measures> {
    measure_id: string;
    measure_name: string;
    values: values[];
    referencess_metadata: referencess_metadata[];
}

import { Model } from 'sequelize-typescript';
export declare class lovs extends Model<lovs> {
    lov_id: string;
    lov_name: string;
    lov_value: string;
}

declare const _default: {
    services: (environment: string, jwt_key: string, jwt_secret: string) => {
        getProduct: (product_id: string, headers: any) => Promise<any>;
    };
};
export default _default;


import { IsNotEmpty, IsString } from 'class-validator';

export class ProductRequest {
    @IsString()
    @IsNotEmpty()
    public eventId: string;

    @IsString()
    @IsNotEmpty()
    public eventType: string;

    @IsString()
    @IsNotEmpty()
    public entityId: string;

    @IsString()
    @IsNotEmpty()
    public entityType: string;

    @IsString()
    @IsNotEmpty()
    public timestamp: string;

    @IsString()
    @IsNotEmpty()
    public datetime: string;

    @IsString()
    @IsNotEmpty()
    public version: string;

    @IsString()
    @IsNotEmpty()
    public country: string;

    @IsString()
    @IsNotEmpty()
    public commerce: string;

    @IsString()
    @IsNotEmpty()
    public channel: string;

    @IsString()
    @IsNotEmpty()
    public domain: string;

    @IsString()
    @IsNotEmpty()
    public capability: string;

    @IsString()
    @IsNotEmpty()
    public mimeType: string;

    @IsString()
    public data: string;

}

import {Injectable,Global,Module} from '@nestjs/common'

@Injectable()
export class UpdateRequest {
    constructor(sku:string, sat: string, um:string){
        this.sku = sku
        this.sat = sat
        this.um = um
    }
    public sku: string;
    public sat: string;  
    public um: string;   
}
// Put ConfigService in a Common Module
@Global()
@Module({
  imports: [],
  providers: [UpdateRequest],
  exports: [UpdateRequest],
})
export class CommonModule

/*export module update {
    export class UpdateRequest {
        constructor(sku:string, sat: string, um:string){
            this.sku = sku
            this.sat = sat
            this.um = um
        }
        public sku: string;
        public sat: string;  
        public um: string;   
    }
}

module.exports = UpdateRequest
  */
import { Column, Model, Table, PrimaryKey, AllowNull } from 'sequelize-typescript';

@Table({
    timestamps: false
})
export class lovs extends Model<lovs> {
    @PrimaryKey
    @AllowNull(false)
    @Column
    lov_id: string;

    @PrimaryKey
    @AllowNull(false)
    @Column
    lov_name: string;

    @Column
    lov_value: string;

}
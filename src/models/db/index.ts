
const fs = require('fs');
const path = require('path');

export default function () {
    let typesdb = {
        types:{},
        entities:{}
    }
    //const basenamedb = path.basename(__filename);
    console.log("dirname",__dirname+'/types/')
    fs.readdirSync(__dirname+'/types/').filter((file: string | string[]) => {
        return (file.indexOf('.') !== 0);
    })
    .forEach((file: string) => {
        console.log("file",path.join(__dirname+'/types/', file))
        let model = require(path.join(__dirname+'/types/', file))
        let filename = file.split('.').slice(0, -1).join('.')
        typesdb.types[filename] = model;
    });
    
    fs.readdirSync(__dirname+'/entities/').filter((file: string | string[]) => {
        return (file.indexOf('.') !== 0);
    })
    .forEach((file: string) => {
        console.log("file",path.join(__dirname+'/entities/', file))
        let model = require(path.join(__dirname+'/entities/', file))
        let filename = file.split('.').slice(0, -1).join('.')
        typesdb.entities[filename] = model;
    });
    
    
    return typesdb;
}

import * as jwt from 'jsonwebtoken';

export function generateJWT(key:string, secret:string, expiration?:number) {
    const accessTokenKey = key;
    const accessTokenSecret = secret;
    const expireTokenSecret = expiration || 3600;  
    return jwt.sign(
      {
        iss: accessTokenKey,
        iat: Math.floor(Date.now() / 1000) - 30,
      },
      accessTokenSecret,
      {
        expiresIn: expireTokenSecret,
        noTimestamp: true,
      },
    );
  }


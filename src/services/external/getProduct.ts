import axios from 'axios';

import { Logger } from '../logger.service';
import { generateJWT } from '../token.service';
 const urls = {
    "qa":"",
    "production":""
}

export class ExternalServices {
    
    static async getProduct(environment:string,jwt_key:string, jwt_secret:string,productId:string, headers:any) {
            console.log("jwtk",jwt_key)
            console.log("jwts",jwt_key)
            const url = `${urls[environment]}${productId}`;
            Logger.info(headers,{url:url})
            const token = generateJWT(jwt_key,jwt_secret)
            const response = await axios.get(url, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-country': headers['x-country'],
                    'X-commerce': headers['x-commerce'],
                    'X-chRef': headers['x-chref'],
                    'X-rhsRef': headers['x-rhsref'],
                    'X-cmRef': headers['x-cmref'],
                    'X-txRef': headers['x-txref'],
                    'X-prRef': headers['x-prref'],
                    'x-usrtx': headers['x-usrtx'],
                     Authorization: `Bearer ${token}`
                }
            });
            return response.data;
    }

}
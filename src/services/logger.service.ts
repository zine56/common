var log = require('npmlog');
const momentTime = require('moment-timezone');
const circularJSON = require('circular-json');
const serviceReference  = process.env.SERVICE_REF;
const nodeReference  = process.env.NODE_REF;
const environment  = process.env.ENVIRONMENT;


const buildMessage = (headers: { [x: string]: any; } | null) => { 
    let messageFinal = "";
    return messageFinal; 
}


export class Logger  {

    static info (headers: any, message: any) {
        const dateFormat = momentTime().tz("America/Santiago").format('YYYY-MM-DD HH:mm:ss.SSS'); 
        const level = 'info';
        log.info(dateFormat ,level, circularJSON.stringify(message), buildMessage(headers));
    }
    static error (headers: any, message: any) {
        const dateFormat = momentTime().tz("America/Santiago").format('YYYY-MM-DD HH:mm:ss.SSS'); 
        const level = 'error';
        log.error(dateFormat ,level, circularJSON.stringify(message), buildMessage(headers));
    }
} 
